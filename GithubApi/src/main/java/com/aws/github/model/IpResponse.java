/**
 * 
 */
package com.aws.github.model;

/**
 * @author Dhruv Bansal
 *
 */
public enum IpResponse {
  LIVE, DOWN, UNKNOWN;
}
