/**
 * 
 */
package com.aws.github.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicHttpRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author Dhruv Bansal
 *
 */
public class Worker implements Callable<WorkerResponse> {

  private static final Pattern urlPattern = Pattern.compile(
      "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
              + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
              + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
      Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);  
  private static final String IPADDRESS_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

  String url;
  ConcurrentHashMap< String, Boolean> status;
  public Worker(String url, ConcurrentHashMap<String, Boolean> processStatus) {
    this.url = url;
    this.status = processStatus;
  }

  @Override
  public WorkerResponse call() throws Exception {
    // TODO Auto-generated method stub
    // http code and return list of IpStatuses
    List<String> urls = new ArrayList<String>();
    List<String> ips = new ArrayList<String>();

    
    String response = "";
    try {
      URL url = new URL(this.url);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("GET");
      BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String inputLine;
      while ((inputLine = in.readLine()) != null) {
        response  = response + inputLine + "\n";
      }
      in.close();
      con.disconnect();
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println(response);
    WorkerResponse wr = new WorkerResponse();
    
    Matcher urlMatcher = urlPattern.matcher(response);
    while (urlMatcher.find()) {
        urls.add(response.substring(urlMatcher.start(1), urlMatcher.end() + 1));
    }

    wr.setUrls(urls);
    Matcher ipMatcher = Pattern.compile(IPADDRESS_PATTERN).matcher(response);
    while (ipMatcher.find()) {
        ips.add(ipMatcher.group());
    }
    
    List<IpStatus> ipstatuses = new ArrayList<IpStatus>();
    for (String ip :ips) {
      try {
        if (status.containsKey(ip)) {
          continue;
        }
        InetAddress geek = InetAddress.getByName(ip);
        if (geek.isReachable(100)) {
          ipstatuses.add(new IpStatus(ip, IpResponse.LIVE.name()));
        } else {
          ipstatuses.add(new IpStatus(ip, IpResponse.DOWN.name()));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (ipstatuses.size() > 0) {
      wr.setStatus(ipstatuses);
    }    
    return wr;
  }
}
