/**
 * 
 */
package com.aws.github.model;

/**
 * @author Dhruv Bansal
 *
 */
public class IpStatus {

  String ip;
  String status;
  /**
   * @param ip2
   * @param string
   */
  public IpStatus(String ip, String status) {
    this.ip = ip;
    this.status = status;
  }
  public String getIp() {
    return ip;
  }
  public void setIp(String ip) {
    this.ip = ip;
  }
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  
}
