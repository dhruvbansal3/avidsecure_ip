package com.aws.github;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.util.Pair;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Stopwatch;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.GroupIdentifier;
import com.amazonaws.services.ec2.model.Reservation;
import com.aws.github.model.AwsMapping;
import com.aws.github.model.Worker;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author dhruvbansal
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class HttpJsonDynamicUnitTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String IPADDRESS_PATTERN =
        
                "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    private static final MediaType CONTENT_TYPE_JSON = MediaType.APPLICATION_JSON_UTF8;
    private static final MediaType CONTENT_TYPE_TEXT = MediaType.TEXT_PLAIN;

    private static HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @Before
    public void getContext() {
        mockMvc = webAppContextSetup(webApplicationContext).build();
        assertNotNull(mockMvc);
    }

    @Autowired
    public void setConverters(HttpMessageConverter<?>[] converters) {
        mappingJackson2HttpMessageConverter = Stream.of(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull(mappingJackson2HttpMessageConverter);
    }

    List<String> httpJsonFiles = new ArrayList<>();
    Map<String, String> httpJsonAndTestname = new HashMap<>();
    Map<String, Long> executionTime = new HashMap<>();
    Map<String, Pair<Pair<String, String>, Pair<String, String>>> testFailures = new HashMap<>();

    @Rule
    public Stopwatch stopwatch = new Stopwatch() {};

    @Rule
    public TestWatcher watchman = new TestWatcher() {
        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }

        @Override
        protected void starting(Description description) {
            super.starting(description);
        }

        @Override
        protected void succeeded(Description description) {
//            generateReportForProperExecution();
        }

        @Override
        protected void failed(Throwable e, Description description) {
//            generateReportForRuntimeFailureExecution();
        }

        @Override
        protected void finished(Description description) {
            super.finished(description);
        }
    };

    @Test
    public void dynamicTests() {
        try {
          InetAddress geek = InetAddress.getByName("124.108.103.104");
          geek.isReachable(100);
          String result = "216.105.38.15\n 216.105.38.16\n";
          Matcher ipMatcher= Pattern.compile(IPADDRESS_PATTERN).matcher(result);
          while (ipMatcher.find()) {
            ipMatcher.group();
            System.out.println(result.substring(ipMatcher.start(1), ipMatcher.end()));
        }          
          ClassPathResource resource = new ClassPathResource("testcases/input.txt");
          List<String> testnames = new ArrayList<>();
          try (InputStream inputStream = resource.getInputStream()) {
              testnames = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                      .lines()
                      .collect(Collectors.toList());
          } catch (IOException ex) {
            ex.printStackTrace();
            throw new Error(ex.toString());
          }
          
          int numberOfThreads = Integer.parseInt(System.getProperty("theads_per_file", "10"));
          ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
          ConcurrentHashMap<String, Boolean> processStatus = new ConcurrentHashMap<String, Boolean>();
//          for (String url : testnames)
//            executor.submit(new Worker(url, processStatus));

          executor.shutdown();
          
          BasicAWSCredentials awsCreds = new BasicAWSCredentials("ABC", "CDE");
          final AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                                .withRegion("ap-southeast-1")
                                .build();
          DescribeInstancesRequest request = new DescribeInstancesRequest();
          boolean done = false;
          List<AwsMapping> mappings = new ArrayList<AwsMapping>();
          while(!done) {
              DescribeInstancesResult response = ec2.describeInstances(request);

              for(Reservation reservation : response.getReservations()) {
                  for(com.amazonaws.services.ec2.model.Instance instance : reservation.getInstances()) {
                    List<GroupIdentifier> groups = instance.getSecurityGroups();
                    System.out.println("id is :- " + instance.getInstanceId());
                    for (GroupIdentifier group : groups) {
                      System.out.println(instance.getInstanceId() + " -- " + group.getGroupId() );
                      mappings.add(new AwsMapping(instance.getInstanceId(), group.getGroupId()));                
                    }

                    
                    
//                      System.out.printf(
//                          "Found instance with id %s, " +
//                          "AMI %s, " +
//                          "type %s, " +
//                          "state %s " +
//                          "and monitoring state %s",
//                          instance.getInstanceId(),
//                          instance.getImageId(),
//                          instance.getInstanceType(),
//                          instance.getState().getName(),
//                          instance.getMonitoring().getState());
                  }
              }

              request.setNextToken(response.getNextToken());

              if(response.getNextToken() == null) {
                  done = true;
              }
          }
        } catch (Exception ex) {
//            throw new Error(ex.toString());
        }
        assertEquals(1l, 1l);
    }

    private void addTestFailure(String filename, Pair<Pair<String, String>, Pair<String, String>> failure) {
        if (testFailures.containsKey(filename)) {
            throw new Error("I should skip rest of the test cases.");
        }

        testFailures.put(filename, failure);
    }

    
}
