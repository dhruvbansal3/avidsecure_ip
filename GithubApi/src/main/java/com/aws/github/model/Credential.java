/**
 * 
 */
package com.aws.github.model;

/**
 * @author Dhruv Bansal
 *
 */
public class Credential {
  
  String aws_access_key_id;
  String aws_secret_access_key;
  String region;
  public String getAws_access_key_id() {
    return aws_access_key_id;
  }
  public void setAws_access_key_id(String aws_access_key_id) {
    this.aws_access_key_id = aws_access_key_id;
  }
  public String getAws_secret_access_key() {
    return aws_secret_access_key;
  }
  public void setAws_secret_access_key(String aws_secret_access_key) {
    this.aws_secret_access_key = aws_secret_access_key;
  }
  public String getRegion() {
    return region;
  }
  public void setRegion(String region) {
    this.region = region;
  }

}
