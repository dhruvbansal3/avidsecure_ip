/**
 * 
 */
package com.aws.github.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.aws.github.model.IpStatus;
import com.aws.github.model.Worker;
import com.aws.github.model.WorkerResponse;
import com.aws.github.repository.IpRepository;
import com.aws.github.services.IpService;
/**
 * @author Dhruv Bansal
 *
 */
@Service
public class IpServiceImpl implements IpService {

  @Autowired
  private IpRepository ipRepository;

  @Override
  public void processFile() {
    ClassPathResource resource = new ClassPathResource("input.txt");
    List<String> testnames = new ArrayList<>();
    try (InputStream inputStream = resource.getInputStream()) {
        testnames = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.toList());
    } catch (IOException ex) {
      ex.printStackTrace();
      throw new Error(ex.toString());
    }
    
    int numberOfThreads = Integer.parseInt(System.getProperty("theads_per_file", "10"));
    ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
    ConcurrentHashMap<String, Boolean> processStatus = new ConcurrentHashMap<String, Boolean>();
    List<Future<WorkerResponse>> list = new ArrayList<Future<WorkerResponse>>();    
    
    BlockingQueue<Future> listTo = new LinkedBlockingQueue<Future>();
    for (String url : testnames) {
      Future<WorkerResponse> future =  executor.submit(new Worker(url, processStatus));
      listTo.add(future);
    }
    List<IpStatus> response = new ArrayList<IpStatus>();
    while(!listTo.isEmpty()) {
      Future f = null;
      try {
        f = listTo.take();
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
      
        try {
          WorkerResponse wr = (WorkerResponse)f.get();
          if (wr.getIpStatus().size() > 0) {
            response.addAll(wr.getIpStatus());
          }
          if (wr.getUrl().size() > 0) {
            for (String url : wr.getUrl()) {
              Future<WorkerResponse> future =  executor.submit(new Worker(url, processStatus));
              listTo.add(future);
            }
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
    }

    ipRepository.save(response);
    executor.shutdown();
    
  }

  @Override
  public void erase() {
    ipRepository.erase();
  }

  /* (non-Javadoc)
   * @see com.aws.github.services.IpService#getIPStatus(java.lang.String)
   */
  @Override
  public String getIPStatus(String iP) {
    return ipRepository.getIPStatus(iP);
  }

  @Override
  public void save(List<IpStatus> ipstatuses) {
    ipRepository.save(ipstatuses);
  }

}
