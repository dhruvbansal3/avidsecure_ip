package com.aws.github.repository;

import java.util.List;

import com.aws.github.model.AwsMapping;
import com.aws.github.model.Instance;
import com.aws.github.model.SecurityGroup;

public interface AWSRepository {

  /**
   * @param groupId
   * @return
   */
  List<Instance> getGroupInstances(String groupId);

  /**
   * @param instanceId
   * @return
   */
  List<SecurityGroup> getInstanceGroups(String instanceId);

  /**
   * @param mapping
   */
  void saveBatch(List<AwsMapping> mapping);

  /**
   * 
   */
  void erase();
}
