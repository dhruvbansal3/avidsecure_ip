package com.aws.github.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Instance {
    private String id;

//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")// "2015-07-15 15:13:31"
//    private Timestamp created_at;

    public Instance() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      Instance other = (Instance) obj;
      if (id == null) {
        if (other.getId() != null)
          return false;
      } else if (!id.equals(other.getId()))
        return false;
      return true;
    }

}
