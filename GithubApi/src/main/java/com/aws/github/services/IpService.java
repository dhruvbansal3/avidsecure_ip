package com.aws.github.services;

import java.util.List;

import com.aws.github.model.IpStatus;


public interface IpService {

  public void processFile();
  /**
   * 
   */
  public void erase();
  /**
   * @param iP
   * @return
   */
  public String getIPStatus(String iP);
  /**
   * @param ipstatuses
   */
  public void save(List<IpStatus> ipstatuses);

}
