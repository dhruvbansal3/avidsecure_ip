package com.aws.github.repository.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aws.github.model.AwsMapping;
import com.aws.github.model.Instance;
import com.aws.github.model.SecurityGroup;
import com.aws.github.repository.AWSRepository;
import com.aws.github.repository.mapper.GroupRowMapper;
import com.aws.github.repository.mapper.InstanceRowMapper;

@Repository
public class AwsRepositoryImpl implements AWSRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public List<Instance> getGroupInstances(String groupId) {
    List<Instance> instances = jdbcTemplate.query("SELECT * FROM awsmapping where group_id=?",
    new Object[] { groupId }, new InstanceRowMapper());
      return instances;
  }

  @Override
  public List<SecurityGroup> getInstanceGroups(String instanceId) {
    List<SecurityGroup> instances = jdbcTemplate.query("SELECT * FROM awsmapping where instance_id=?",
    new Object[] { instanceId }, new GroupRowMapper());
      return instances;
  }

  @Override
  public void saveBatch(final List<AwsMapping> mappings) {
      final int batchSize = Integer.parseInt(System.getProperty("INSERT_BATCH_SIZE", "500"));

      for (int j = 0; j < mappings.size(); j += batchSize) {

          final List<AwsMapping> batchList = mappings.subList(j, j + batchSize > mappings.size() ? mappings.size() : j + batchSize);
          String sql = "INSERT INTO awsmapping " +
              "(instance_id, group_id) VALUES (?, ?)";

          jdbcTemplate.batchUpdate(sql,
              new BatchPreparedStatementSetter() {
                  @Override
                  public void setValues(PreparedStatement ps, int i)
                          throws SQLException {
                      AwsMapping mapping = batchList.get(i);
                      ps.setString(1, mapping.getInstance_id());
                      ps.setString(2, mapping.getGroup_id());
                  }

                  @Override
                  public int getBatchSize() {
                      return batchList.size();
                  }
              });

      }
  }

  @Override
  public void erase() {
    jdbcTemplate.update("delete from awsmapping");
  }
}
