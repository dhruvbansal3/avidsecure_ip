/**
 * 
 */
package com.aws.github.exceptions;

/**
 * @author Dhruv Bansal
 *
 */
public class NotFoundException extends Exception {
  public NotFoundException() {
    super();
  }
}
