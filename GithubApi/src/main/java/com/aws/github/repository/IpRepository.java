/**
 * 
 */
package com.aws.github.repository;

import java.util.List;

import com.aws.github.model.IpStatus;

/**
 * @author Dhruv Bansal
 *
 */
public interface IpRepository {
  
  public void save(List<IpStatus> statuses);
  
  public void erase();

  /**
   * @param iP
   * @return
   */
  public String getIPStatus(String iP);

}
