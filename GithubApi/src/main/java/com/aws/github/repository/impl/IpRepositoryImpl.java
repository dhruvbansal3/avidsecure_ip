/**
 * 
 */
package com.aws.github.repository.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aws.github.model.IpResponse;
import com.aws.github.model.IpStatus;
import com.aws.github.repository.IpRepository;

/**
 * @author Dhruv Bansal
 *
 */
@Repository
public class IpRepositoryImpl implements IpRepository{

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public void save(List<IpStatus> statuses) {
    final int batchSize = Integer.parseInt(System.getProperty("INSERT_BATCH_SIZE", "500"));

    for (int j = 0; j < statuses.size(); j += batchSize) {

        final List<IpStatus> batchList = statuses.subList(j, j + batchSize > statuses.size() ? statuses.size() : j + batchSize);
        String sql = "INSERT INTO ipstatus " +
            "(ip, status) VALUES (?, ?)";

        jdbcTemplate.batchUpdate(sql,
            new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i)
                        throws SQLException {
                  IpStatus ipStatus = batchList.get(i);
                    ps.setString(1, ipStatus.getIp());
                    ps.setString(2, ipStatus.getStatus());
                }

                @Override
                public int getBatchSize() {
                    return batchList.size();
                }
            });

    }

  }

  @Override
  public void erase() {
    jdbcTemplate.update("delete from ipstatus");
  }

  /* (non-Javadoc)
   * @see com.aws.github.repository.IpRepository#getIPStatus(java.lang.String)
   */
  @Override
  public String getIPStatus(String iP) {
    String ipStatus;
    try {
      ipStatus = jdbcTemplate.queryForObject(
          "SELECT status FROM ipstatus where ip=?", new Object[] { iP }, String.class);
    } catch (EmptyResultDataAccessException e) {
      return IpResponse.UNKNOWN.name();
    }
    return ipStatus;
  }

}
