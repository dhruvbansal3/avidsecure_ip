/**
 * 
 */
package com.aws.github.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dhruv Bansal
 *
 */
public class WorkerResponse {

  List<String> urls = new ArrayList<String>();
  List<IpStatus> statuses = new ArrayList<IpStatus>();
  
  public void setUrls(List<String> urls) {
    this.urls = urls;
  }
  
  public void setStatus(List<IpStatus> statuses) {
    this.statuses = statuses;
  }
  
  public List<String> getUrl() {
    return urls;
  }

  public List<IpStatus> getIpStatus() {
    return statuses;
  }

}
